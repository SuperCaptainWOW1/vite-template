export default class Component extends HTMLElement {
  constructor() {
    super();

    this.props = {};
    this.html = '';
    this.rootElement = null;
    this.css = '';

    // Get all props
    this.getAttributeNames().forEach(prop => {
      this.props[prop] = this.getAttribute(prop);
    });

    // Create shadow DOM
    this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    if (template.children.length > 1) {
      throw new Error('Template element must have only one root element');
    } else {
      // Create template
      const template = document.createElement('template');
      // Fill the template CSS
      template.innerHTML = this.css;
      // Fill the template HTML
      template.innerHTML += this.html;

      // Get root element
      this.rootElement = template.children[0];
      // Append root element to shadow DOM
      this.shadowRoot.append(template.content.cloneNode(true));
    }
  }
}
