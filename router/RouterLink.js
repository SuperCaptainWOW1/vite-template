import Component from '../lib/Component';

class RouterLink extends Component {
  constructor() {
    super();

    console.log('Props', this.props);

    this.html = `
      <a href="${this.props.to}">Link</a>
    `;

    console.log('rootElement contrsuctor', this.rootElement);

    this.css = `
      <style>
        a {
          color: 'orange';
        }
      </style>
    `;
  }

  connectedCallback() {
    console.log('rootElement', this.rootElement);
  }
}

window.customElements.define('router-link', RouterLink);
